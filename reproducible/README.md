# Reproducible Research
- [Reproducible Research Lecture Notes](reproducible_research_lecture.md)

## Git
- [Overview](git_overview.md)
- Remotes
  -  [Setup Remote Repo on GitLab](reproducible/git_remote_setup_gitlab.Rmd)
  -  [Setup Remote Repo on GitHub](reproducible/git_remote_setup_github.Rmd)
- [Conflicts](git_conflicts.md)
- [Team Exercise](git_team_exercise.md)
- [Cloning and Forking](git_cloning_and_forking.md)
- [Branching, Stashing, Tagging](branch_stash_tag.md)
