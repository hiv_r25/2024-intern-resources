# Why?

## Equations

This:

    $$
    \begin{equation}
    \frac{x^2}{x^3+x^5}
    \end{equation}
    $$

Produces this:
$$
\\begin{equation}
\\frac{x^2}{x^3+x^5}
\\end{equation}
$$

## Reproducible: Literate Programming

This slide shows how you can mix code, results and descriptive text.

    plot(pressure)

![](presentations_rmarkdown_files/figure-markdown_strict/pressure-1.png)

## Reproducible: Tables

    iris2 <- head(iris)
    knitr::kable(iris2, col.names = gsub("[.]", " ", names(iris)))

<table>
<thead>
<tr class="header">
<th style="text-align: right;">Sepal Length</th>
<th style="text-align: right;">Sepal Width</th>
<th style="text-align: right;">Petal Length</th>
<th style="text-align: right;">Petal Width</th>
<th style="text-align: left;">Species</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: right;">5.1</td>
<td style="text-align: right;">3.5</td>
<td style="text-align: right;">1.4</td>
<td style="text-align: right;">0.2</td>
<td style="text-align: left;">setosa</td>
</tr>
<tr class="even">
<td style="text-align: right;">4.9</td>
<td style="text-align: right;">3.0</td>
<td style="text-align: right;">1.4</td>
<td style="text-align: right;">0.2</td>
<td style="text-align: left;">setosa</td>
</tr>
<tr class="odd">
<td style="text-align: right;">4.7</td>
<td style="text-align: right;">3.2</td>
<td style="text-align: right;">1.3</td>
<td style="text-align: right;">0.2</td>
<td style="text-align: left;">setosa</td>
</tr>
<tr class="even">
<td style="text-align: right;">4.6</td>
<td style="text-align: right;">3.1</td>
<td style="text-align: right;">1.5</td>
<td style="text-align: right;">0.2</td>
<td style="text-align: left;">setosa</td>
</tr>
<tr class="odd">
<td style="text-align: right;">5.0</td>
<td style="text-align: right;">3.6</td>
<td style="text-align: right;">1.4</td>
<td style="text-align: right;">0.2</td>
<td style="text-align: left;">setosa</td>
</tr>
<tr class="even">
<td style="text-align: right;">5.4</td>
<td style="text-align: right;">3.9</td>
<td style="text-align: right;">1.7</td>
<td style="text-align: right;">0.4</td>
<td style="text-align: left;">setosa</td>
</tr>
</tbody>
</table>

## Reproducible: Version Control

<img src="./git_diff.png" width="1572" />

## Combining languages

### R

    my_range <- seq(from = 1, to = 4, by = 1)
    print(my_range)

    ## [1] 1 2 3 4

### Python

    my_range = list(range(1, 5))
    print(my_range)

    ## [1, 2, 3, 4]

## Quick and Easy

# How?

## Quick Start

1.  Open a new RMarkdown File: File -&gt; New File -&gt; Rmarkdown
2.  In *New R Markdown* dialog, select *Presentation* and click *OK*
3.  Edit
4.  Click on *Knit*

## Some Details

-   The default output is *ioslides*.
-   There is [detailed information on ioslides
    here](https://bookdown.org/yihui/rmarkdown/ioslides-presentation.html)
-   Control the presentation with arrow keys
-   Knitting generates the presentation as an HTML file, which can be
    downloaded and run in any webbrowser

# Generating Other Formats

## Presentation Formats

Click on the triangle next to *Knit* and select one of these other
formats

-   Slidy
-   Beamer (requires LaTeX)
-   Powerpoint
    -   I needed to manually add `powerpoint_presentation: default` to
        the YAML to get this to work

There are detailed instructions on each of these formats at
<https://rmarkdown.rstudio.com/lesson-11.html>

## Reports

-   HTML: add `html_document: default` to YAML, then select “Knit to
    html\_document” from the Knit menu
-   Markdown: add `md_document: default` to YAML, then select “Knit to
    md\_document” from the Knit menu

## R Presentations

There is another mechanism for [R Presentations described
here](https://support.rstudio.com/hc/en-us/articles/200486468-Authoring-R-Presentations)
