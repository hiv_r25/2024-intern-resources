This website can be reached at: https://duke.is/r25intern

# Orientation Parts
- [Reproducible Analysis](reproducible/README.md)


# Computing Environments
- [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
    - [Getting started with DCC OnDemand: RStudio](misc/rstudio_ondemand_howto.md)
    - [Getting started with DCC OnDemand: Jupyter](misc/jupyter_ondemand_howto.md)

# Orientation Content
- [Initial download of workshop content](misc/git_cloning.md)
- [Update workshop content](misc/git_pull.md)

# Misc
- Creating Presentaions in Rmarkdown
  - [Rmd](literate_programming/presentations_rmarkdown.Rmd): Click on "</>" to see raw Rmarkdown
  - [md](literate_programming/presentations_rmarkdown.md)
