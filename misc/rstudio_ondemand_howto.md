# Starting up RStudio on DCC OnDemand
1. Go to [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
2. Sign in with your NetID and password
3. Click on **Interactive Apps** at the top and select **CHSI RStudio Workshop** 
    - If you don't see **Interactive Apps** and only see three lines in the top right, click on the three lines, then **Interactive Apps**, then **CHSI RStudio Workshop**
4. A new page should open that says **CHSI RStudio Workshop** at the top.
    - For *Account* select "hivr45-interns-2024" (yes, the account actually does have a spelling mitake)
    - For *Partition* select "chsi"
    - For *Walltime* select "4"
    - For *Singularity Image*, be sure that *R25 HIV/AIDS Workshop" is selected
    - Leave everything else as it is
    - Scroll to the bottom and click **Launch**
5. A new page should open with a box that says **CHSI RStudio Workshop**, you may see the following messages:
    - "Please be patient as your job currently sits in queue. The wait time depends on the number of cores as well as time requested."
    - "Your session is currently starting... Please be patient as this process can take a few minutes."
6. You are waiting for a blue button to appear that says **Connect to RStudio Server**. This could take a minute or so. When it appears, click on it.
7. After a minute or so, RStudio should open in your webbrowser.
8. Please continue with the instructions in the next section to clean up.

# Shutting Down
1. In RStudio click on **File** in the top left, then select **Quit Session**
2. A box should pop up that says **R Session Ended**. At this point you can close the tab that contains the RStudio session.
3. In the DCC OnDemand browser window click on the red **Delete** button in the **RStudio** box, then click **Confirm** in the box that pops up.
4. You can now click on the logout button in the top right, which is an arrow pointing to the right.
5. You are done!
